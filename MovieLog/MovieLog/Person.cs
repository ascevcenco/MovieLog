﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLog
{
    class Person
    {
        private int _id;
        public string FirstName;
        public string MiddleName;
        public string Surname;
        public bool Gender;
        public DateTime Birthday;
        public string Birthplace;
        public string Occupation;
        public string Photo;


        public Person(string FirstName, string MiddleName, string Surname, bool Gender, DateTime Birthday,
                      string Birthplace, string Occupation, string Photo)
        {
            this.FirstName = FirstName;
            this.MiddleName = MiddleName;
            this.Surname = Surname;
            this.Gender = Gender;
            this.Birthday = Birthday;
            this.Birthplace = Birthplace;
            this.Occupation = Occupation;
            this.Photo = Photo;
        }

        public Person(string FirstName, string MiddleName, string Surname, bool Gender, DateTime Birthday,
                      string Birthplace, string Occupation) :
            this(FirstName, MiddleName, Surname, Gender, Birthday, Birthplace, Occupation, "")
        {

        }
    }
}
