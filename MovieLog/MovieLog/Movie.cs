﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieLog
{
    class Movie
    {
        private int _id;
        public string Title;
        public string Director;
        private int _yearOfProduction;
        public string CountryOfProduction;
        public int Length;
        public string LeadingActors;
        public string Genre;
        public string Poster;


        public Movie(string Title, string Director, int _yearOfProduction, string CountryOfProduction, 
                      int Length, string LeadingActors, string Genre, string Poster)
        {
            this.Title = Title;
            this.Director = Director;
            this._yearOfProduction = _yearOfProduction;
            this.CountryOfProduction = CountryOfProduction;
            this.Length = Length;
            this.LeadingActors = LeadingActors;
            this.Genre = Genre;
            this.Poster = Poster;
        }

        public Movie(string Title, string Director, int _yearOfProduction, string CountryOfProduction, 
                      int Length, string LeadingActors, string Genre):
               this(Title, Director, _yearOfProduction, CountryOfProduction, Length, LeadingActors, Genre, "")
        {

        }
    }
}
